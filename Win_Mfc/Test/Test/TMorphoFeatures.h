#pragma once

#include "opencv2/highgui/highgui.hpp"   
#include "opencv2/opencv.hpp" 

class TMorphoFeatures
{
private:
	int threshold;
	cv::Mat cross;
	cv::Mat diamond;
	cv::Mat square;
	cv::Mat x;


	void applyThreshold(cv::Mat& result);
public:
	TMorphoFeatures(void);
	~TMorphoFeatures(void);

	void   setThreshold(int t); 
	cv::Mat getEdges(const cv::Mat& image);
	
	cv::Mat getCorners(const cv::Mat& image);
	cv::Mat drawOnIamge(const cv::Mat& binary, cv::Mat& image);
};

