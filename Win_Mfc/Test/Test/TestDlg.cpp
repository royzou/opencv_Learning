
// TestDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Test.h"
#include "TestDlg.h"
#include "afxdialogex.h"

#include "Histogram.h" 
#include "CalcThreshold.h"
#include "HistImageProcessing.h"
#include "TWatershedPro.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTestDlg 对话框




CTestDlg::CTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CTestDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CTestDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTestDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CTestDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CTestDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CTestDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CTestDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CTestDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CTestDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CTestDlg::OnBnClickedButton9)
END_MESSAGE_MAP()


// CTestDlg 消息处理程序

BOOL CTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTestDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();



}



void CTestDlg::OnBnClickedButton1()
{
	using namespace cv;
	using namespace std;
   // 设置过滤器   
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}

	Thistogram  histogram;
	Mat histImage = histogram.getHistogramImage(src);
	imshow("直方图", histImage);

}


CString CTestDlg::GetDlgFileName(void)
{
	 // 设置过滤器   
    TCHAR szFilter[] = _T("jpg文件(*.jpg)|*.jpg|bmp文件(*.bmp)|*.bmp|所有文件(*.*)|*.*||");   
    // 构造打开文件对话框   
    CFileDialog fileDlg(TRUE, _T("jpg"), NULL, 0, szFilter, this);   
    CString strFilePath;   
   
	 // 显示打开文件对话框   
    if (IDOK == fileDlg.DoModal())   
    {   
        // 如果点击了文件对话框上的“打开”按钮，则将选择的文件路径显示到编辑框里   
        strFilePath = fileDlg.GetPathName();   
    }else{
		strFilePath = "";
	}

	return strFilePath;
}


void CTestDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
		// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
   // 设置过滤器   
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	if (!strFileName.empty() ){
        Mat src = imread(strFileName);//以灰度形式读取图片 
		imshow("src", src);
	}
	    
	
/*
    Mat dst;      
	Canny(src, dst, 59, 150, 3);
  //  imwrite("D:\\MCode\\Test_image\\GluePlan\\10.bmp",dst); //保存文件，梯度图  
      
   imshow("dst", dst);  
    waitKey(0);  */
}


void CTestDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
   // 设置过滤器   
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}

	Thistogram  histogram;
	Mat histImage = histogram.getHistogramImage(src);
	Mat InvImage = histogram.InvImage(src); 
	long histval[256];
	histogram.GetHistVal(src, histval);
	imshow("直方图", histImage);
	imshow("反向图", InvImage);

	TCalcThreshold CalcThreshold(histval);
	long ThrVal = CalcThreshold.GetThreshold(OSTUThreshold);

	Mat ThresholdImage;
	threshold(src, ThresholdImage, ThrVal, 255, THRESH_BINARY);
	imshow("二值图", ThresholdImage);


	Mat ThresholdImage1;
	threshold(src, ThresholdImage1, 0, 255, THRESH_BINARY_INV);
	imshow("二值图1", ThresholdImage1);
}


void CTestDlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
   // 设置过滤器   
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_COLOR);//以灰度形式读取图片 
		imshow("彩色图", src);
	}

	

	TColorHistogram  ColorHistogram;
	
	std::vector<cv::Mat> histImage = ColorHistogram.getHistogramImage(src);
	imshow("彩色直方图B", histImage[0]);
	imshow("彩色直方图G", histImage[1]);
	imshow("彩色直方图R", histImage[2]);
}


void CTestDlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
   // 设置过滤器   
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}

	THistImageProcessing HistImageProcessing;
	cv::Mat stretchImage = HistImageProcessing.Stretch(src, 100);
	imshow("增强图", stretchImage);

}


void CTestDlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
   // 设置过滤器   
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}

	THistImageProcessing HistImageProcessing;
	cv::Mat stretchImage = HistImageProcessing.equalize(src);
	imshow("均衡化", stretchImage);

	Thistogram  histogram;
	imshow("灰度图直方图",  histogram.getHistogramImage(src));
	imshow("均衡化后直方图",  histogram.getHistogramImage(stretchImage));
}


void CTestDlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}

	THistImageProcessing HistImageProcessing;
	cv::Mat Image = HistImageProcessing.BackProject(src, Rect(500,220,70,50), (float)0.1);
	imshow("反投影", Image);


}


void CTestDlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}


	cv::Mat imageRoi = src(cv::Rect(500,220,70,50));
	Thistogram  histogram;
	cv::MatND hist = histogram.getHistogram(imageRoi);

	TContentFinder ContentFinder;
	ContentFinder.setHistogram(hist);
	ContentFinder.setThreshold(0.05f);

	int channels[3] = {0,1,2};
	Mat result = ContentFinder.find(src,0,255,channels,3);

	imshow("反投影", result);
}


void CTestDlg::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	using namespace cv;
	using namespace std;
	CString CstrFileName =  GetDlgFileName();
	string strFileName = CstrFileName.GetBuffer(0);  
	Mat src;
	if (!strFileName.empty() ){
        src = imread(strFileName,IMREAD_GRAYSCALE);//以灰度形式读取图片 
		imshow("灰度图", src);
	}

	TWatershedPro WatershedPro;
	WatershedPro.setMarkers(src);
	WatershedPro.process(src);	

	imshow("分水岭",src);
}
