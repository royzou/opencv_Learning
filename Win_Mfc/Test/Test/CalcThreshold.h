#pragma once



enum EnumThreshold
{
	MeanThreshold, 
	PTileThreshold, 	MinimumThreshold,
	IntermodesThreshold,
	IterativeBestThreshold,
	OSTUThreshold,
	OneDMaxEntropyThreshold,
	MomentPreservingThreshold,
	HuangFuzzyThreshold,
	KittlerMinError,
	IsoDataThreshold,
	ShanbhagThreshold,
	YenThreshold,

	NotProcessImageBinarize
};


class TCalcThreshold
{
private:
	long Threshold;
	long HistVal[256];
private:
    void  CalculateThreshold(EnumThreshold ThresholdMethod);
	long  GetMeanThreshold(long HistGram[]);
    long  GetPTileThreshold(long HistGram[], int Tile = 50);
	long  GetMinimumThreshold(long HistGram[]);
    long  GetIntermodesThreshold(long HistGram[]);
    bool  IsDimodal(double HistGram[]);
    long  GetIterativeBestThreshold(long HistGram[]);
    long  GetOSTUThreshold(long HistGram[]);
    long  Get1DMaxEntropyThreshold(long HistGram[]);
    unsigned char  GetMomentPreservingThreshold(long HistGram[]);
    double  A(long HistGram[], int Index);
    double  B(long HistGram[], int Index);
    double  C(long HistGram[], int Index);
    double  D(long HistGram[], int Index);
    long  GetHuangFuzzyThreshold(long HistGram[]);
    long  GetKittlerMinError(long HistGram[]);
    long  GetIsoDataThreshold(long HistGram[]);
    long  GetShanbhagThreshold(long HistGram[]);
    long  GetYenThreshold(long HistGram[]);
public:
	TCalcThreshold(long* HistVal);
	~TCalcThreshold(void);

	long  GetThreshold(EnumThreshold ThresholdMethod);
};

