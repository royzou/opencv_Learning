#pragma once

#include "opencv2/highgui/highgui.hpp"   
#include "opencv2/opencv.hpp" 

class TWatershedPro
{
private:
	cv::Mat markers;

public:
	TWatershedPro(void);
	~TWatershedPro(void);

	void   setMarkers(const cv::Mat& markerImage);
    cv::Mat process(const cv::Mat& image);

	
};


/*
TWatershedPro WatershedPro;
WatershedPro.setMarkers(markers);
WatershedPro.process(image);
*/

