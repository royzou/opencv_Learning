#pragma once

#include "opencv2/highgui/highgui.hpp"   
#include "opencv2/opencv.hpp" 

class Thistogram
{
private:
	int histSize[1];
	float hranges[2];
	const float* ranges[1];
	int channels[1];
public:
	Thistogram(void);
	~Thistogram(void);

    cv::MatND getHistogram(const cv::Mat& image);
    cv::Mat   getHistogramImage(const cv::Mat& image);

	cv::Mat   applyLiikUp(const cv::Mat& image,const cv::Mat& lookup);
	cv::Mat   InvImage(const cv::Mat& image);
	
	

	void GetHistVal(const cv::Mat& image,  long* lhistVal, float* fhistVal = NULL);
};


class TColorHistogram
{
private:
	int histSize[3];
	float hranges[2];
	const float* ranges[3];
	int channels[3];
public:
	TColorHistogram(void);
	~TColorHistogram(void);

    cv::MatND getHistogram(const cv::Mat& image);
    cv::SparseMat getSparseHistogram(const cv::Mat& image);
    std::vector<cv::Mat>   getHistogramImage(const cv::Mat& image);
	std::vector<cv::Mat>   getSparseHistogramImage(const cv::Mat& image);


};

