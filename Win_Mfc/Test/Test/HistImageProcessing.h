#pragma once

#include "opencv2/highgui/highgui.hpp"   
#include "opencv2/opencv.hpp" 
#include "Histogram.h"

class THistImageProcessing
{
private:
	float hranges[2];
	const float* ranges[1];
	int channels[1];
public:
	THistImageProcessing(void);
	~THistImageProcessing(void);


	cv::Mat   Stretch(const cv::Mat& image, int minValue=0);   //直方图拉伸
	cv::Mat   equalize(const cv::Mat& image);   //直方图均衡化
	cv::Mat   BackProject(cv::Mat Image, const cv::Rect& imageRoiRect, const float& thres);   //反向投影

};


class TContentFinder
{
private:
	float hranges[2];
	const float* ranges[3];
	int channels[3];
	float threshold;
	cv::MatND histogram;
public:
	TContentFinder(void);
	~TContentFinder(void);


	void   setThreshold(float t);   //0--1
	float   getThreshole();   //
	void   setHistogram(const cv::MatND& h);   //
	cv::Mat find(const cv::Mat& image,
				float minValue, 
				float maxValue,
				int* channels, 
				int dim);

};






