#include "StdAfx.h"
#include "HistImageProcessing.h"


THistImageProcessing::THistImageProcessing(void)
{
	hranges[0] = 0.0;
	hranges[1] = 255.0;
	ranges[0] = hranges;
	channels[0] = 0;
}


THistImageProcessing::~THistImageProcessing(void)
{
}



//-------------------------------------------------------------------------------
cv::Mat   THistImageProcessing::Stretch(const cv::Mat& image, int minValue)
{
	Thistogram HistImage;	
	cv::MatND hist = HistImage.getHistogram(image);

	cv::Mat result;
	if(image.type() != CV_8U) return result;
	int histSize = 256;

	int imin =0;
	for(;imin<histSize;imin++){
		if(hist.at<float>(imin) > minValue){
			
			break;
		}
	}

	int imax = histSize - 1;
	for(;imax>=0;imax--){
		if(hist.at<float>(imax) > minValue){
			
			break;
		}
	}

	int dim(256);
	cv::Mat lookup(1,&dim,CV_8U);
	for(int i=0;i<256;i++){
		if(i<imin) lookup.at<uchar>(i)=0;
		else if(i>imax) lookup.at<uchar>(i)=255;
		else lookup.at<uchar>(i) = static_cast<uchar>(255.0*(i-imin)/(imax-imin)+0.5);
	}


	cv::LUT(image, lookup, result);	
	return result;
}

//-------------------------------------------------------------------------------
cv::Mat   THistImageProcessing::equalize(const cv::Mat& image)
{
	cv::Mat result;
	cv::equalizeHist(image,result);
	return result;
}
//-------------------------------------------------------------------------------
cv::Mat  THistImageProcessing::BackProject(cv::Mat Image,const cv::Rect& imageRoiRect, const float& thres)
{
	cv::Mat imageRoi;
	imageRoi  = Image(imageRoiRect);

	Thistogram HistImage;	
	cv::MatND hist = HistImage.getHistogram(imageRoi);

	cv::normalize(hist, hist, 1.0);

	cv::Mat result;
	cv::calcBackProject(&Image, 1, channels, hist, result, ranges, 255.0);
	cv::threshold(result, result, 255*thres, 255, cv::THRESH_BINARY);

	return result;
}
//-------------------------------------------------------------------------------

















//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
TContentFinder::TContentFinder(void)
	:threshold(-1.0f)
{
	ranges[0] = hranges;
	ranges[1] = hranges;
	ranges[2] = hranges;
}
//-------------------------------------------------------------------------------
TContentFinder::~TContentFinder(void)
{
}
//-------------------------------------------------------------------------------


void   TContentFinder::setThreshold(float t)
{
	threshold = t;
}
//------------------------------------------------------------------------------- 
float   TContentFinder::getThreshole()
{
	return threshold;
}
//-------------------------------------------------------------------------------
void   TContentFinder::setHistogram(const cv::MatND& h)
{
	histogram = h;
	cv::normalize(histogram,histogram,1.0);
}
//-------------------------------------------------------------------------------
cv::Mat TContentFinder::find(const cv::Mat& image,float minValue,float maxValue,int* channels,int dim)
{
	cv::Mat result;
	hranges[0] = minValue;
	hranges[1] = maxValue;
	for(int i=0;i<dim;i++)
		this->channels[i] = channels[i];
	cv::calcBackProject(&image, 1,
						channels,
						histogram,
						result,
						ranges,
						255.0
						);
	if(threshold>0.0)
		cv::threshold(result, result, 255*threshold, 255, cv::THRESH_BINARY);
	return result;
}
//-------------------------------------------------------------------------------