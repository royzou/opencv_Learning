#include "StdAfx.h"
#include "TWatershedPro.h"

//-------------------------------------------------------
TWatershedPro::TWatershedPro(void)
{

}
//-------------------------------------------------
TWatershedPro::~TWatershedPro(void)
{

}
//-------------------------------------------------
void   TWatershedPro::setMarkers(const cv::Mat& markerImage)
{
    markerImage.convertTo(markers, CV_32S);
}
//-------------------------------------------------
cv::Mat TWatershedPro::process(const cv::Mat& image)
{
    cv::watershed(image, markers);
    return markers;
}
//-------------------------------------------------